# PrjctPlannr

## Challenge

Our PM Office asked you as the newly joined member of the Development team for help in scheduling projects and assigning the right resources. As a preparation for the next year you should help with the planning of assignments. And since you don't want to do all of the work manually you decided to write a program that will do the job for you – in an ideal way.

Consider you have a number of developers, testers and ops people, e.g.:

* `Carl:Java`
* `Lenny:Java`
* `Bart:JavaScript`
* `Moe:JavaScript`
* `Milhouse:.net`
* `Lisa:.net`
* `Ned:QA`
* `Edna:QA`
* `Barney:Ops`
* `Homer:Ops`

as well as a list of projects given like this:

* `PIM (2x.net, 1xJavaScript) - 10 weeks`
* `DAM (2xJava, 1xJavaScript) - 8 weeks`

Then your task is to plan the resources this way, that usage of the people is optimized and projects are finished as early as possible. Meaning that neither devs, qa or ops should sit and do nothing. Also note: each project needs exactly 1 QA and 1 Ops person for the full time. So with the given input from above one possible solution could be:

```
PIM
  start: 1st week of JAN
  end:   2nd week of MAR
  team:  Lisa, Milhouse, Bart, Ned, Homer
  
DAM
  start: 1st week of JAN
  end:   4th week of FEB
  team:  Carl, Lenny, Moe, Edna, Barney
```

To ease up things you may assume that:

* each project will need exactly **one** QA and **one** Ops person for the full time
* names will be unique and do not appear twice or more often
* one month always is exaclty **four** weeks long, so the year is just 48 instead of 52 weeks (whereas this is only true for February)
* people can be allocated freely and there are no teams
* productivity of individuals is equally good – regardless who the do work with
* order of projects is not important – there are no dependencies
* project only can start if they are fully staffed
* projects that can't be finished by end of December should not be started. In this case print them out at the end with a hint why they'll not be done, e.g.

```
ABC
  not feasible: no time 
  
XYZ
  not feasible: no skills
```

Your program has to work on the command line and expect the input in the following format:

1. `PEOPLE`
2. List of people as stated above
3. empty line
4. `PROJECTS`
5. List of project as stated above
6. empty line

So e.g.:


    PEOPLE
    Carl:Java
    Lenny:Java
    Bart:JavaScript
    Moe:JavaScript
    Milhouse:.net
    Lisa:.net
    Ned:QA
    Edna:QA
    Barney:Ops
    Homer:Ops

    PROJECTS
    PIM (2x.net, 1xJavaScript) - 10 weeks
    DAM (2xJava, 1xJavaScript) - 8 weeks
    ABC (2xJava, 1xJavaScript) - 41 weeks
    XYZ (1xRuby) - 3 weeks
     
Afterwards your output should looke like:

```
PIM
  start: 1st week of JAN
  end:   2nd week of MAR
  team:  Lisa, Milhouse, Bart, Ned, Homer
  
DAM
  start: 1st week of JAN
  end:   4th week of FEB
  team:  Carl, Lenny, Moe, Edna, Barney

ABC
  not feasible: no time
  
XYZ
  not feasible: no skills
 
```

## Rules

1. Develop this application as a Java command line application. It doesn't matter if the application reads the files on its own or gets it passed from STDIN. It is perfectly acceptable if it is started using `java -jar app.jar input.txt` or `cat input.txt | java -jar app.jar`
2. Package it up into a self running, all dependency containing jar file.
3. Create a `README.md` explaining how to build/run/use the app.
4. You shouldn't use any framework that solves the task for you but helping ones like Testing or Command Line Wrappers are allowed. In any case name every framework you use in your `README.md` and state what it does any why you used it.
5. Every person having Java and some standard tools (Ant, Maven, Gradle) should be able to check out the code, build and run the app.
6. Think about good design. What do you do to ensure reusability as well as maintainability.
7. Think about how you are ensuring quality assurance in your process.
8. If you're done check in your solution into any public git repo hoster (github, bitbucket, etc.) and send us the link to GlobalSoftwareDevelopment@adidas-group.com